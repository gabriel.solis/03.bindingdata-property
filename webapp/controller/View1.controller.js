sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/Input",
	"sap/ui/model/type/Float"
], function (Controller, Input,Float) {
	"use strict";

	return Controller.extend("Tutoriales.DataBinding.controller.View1", {
		onInit: function () {
			var oInput = new Input({
				value: "{/company/name}"
			});

			var oInput2 = new Input();

			var oVerticalLayout = this.getView().byId("verticalL");
			oVerticalLayout.addContent(oInput);

			//Set property  model using the bindProperty method:
			oInput2.bindProperty("value", "/company/name");
			oVerticalLayout.addContent(oInput2);

			//Format property value
			var oInputFormat = new Input({
				value: {
					path: "/companyname",
					formatter: function (fValue) {
						if (fValue) {
							return "> " + Math.floor(fValue / 1000000) + "M";
						}
						return "0";
					}
				}
			});

			oVerticalLayout.addContent(oInputFormat);


			var oControl = new sap.m.Input({
				value: {
					path: "/company/revenue",
					type: new Float({
						minFractionDigits: 2,
						maxFractionDigits: 2
					})
				}
			});
			
			oVerticalLayout.addContent(oControl);
		},
		roundToMillion: function (fValue) {
			if (fValue) {
				return "> " + Math.floor(fValue / 1000000) + "M";
			}
			return "0";
		}
	});
});